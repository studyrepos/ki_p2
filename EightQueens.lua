EightQueen = {}
local EightQueen_mt = Class(EightQueen)

function EightQueen:new(string)
  local queens = {}
  local fitness = 0
  
  if string then
    local n
    for i=1, 8 do
      n = tonumber(string:sub(i,i))
      if n == nil or n < 1 or n > 8 then
        print('Invalid position at ' .. i)
      else
        queens[i] = n
      end
    end
  end

  return setmetatable({queens = queens, fitness = fitness}, EightQueen_mt)
end

function EightQueen:moveQueen(col, pos)
  if (col >= 1) and (col <= 8) and (pos >= 1) and (pos <= 8) then
    self.queens[col] = pos
    self:calcFitness()
  else
    print('Invalid Position: [' .. col .. '|' .. pos .. ']')
  end
end

function EightQueen:stringToQueens(string)
  local n
  for i=1, 8 do
    n = tonumber(string:sub(i,i))
    if n == nil or n < 1 or n > 8 then
      print('Invalid position at '.. i)
      return false
    end
    self.queens[i] = n
  end
  return true
end

function EightQueen:queensToString()
  local s = ''
  for i=1, 8 do
    s = s .. self.queens[i]
  end
  return s
end

function EightQueen:toString()
  return self.fitness .. "-" .. self:queensToString()
end

-- Calculates Fitness of QueensSetting
function EightQueen:calcFitness()
  local clashes = 0
  local dx, dy
  for i=1,8 do
    for j=i+1,8 do
      dx = math.abs(i-j)
      dy = math.abs(self.queens[i] - self.queens[j])
      if (dx == dy) or (dy == 0) then
        clashes = clashes + 1
      end
    end
  end
  self.fitness = 28 - clashes
end

-- Checks QueensPosition only on left side, ignoring all queens to the right
-- used for BacktrackingSearch
function EightQueen:isSafe(col, pos)
  local dx, dy
  for i=1, col-1 do
    dx = math.abs(col-i)
    dy = math.abs(pos - self.queens[i])
    if (dx == dy) or (dy == 0) then
      return false
    end
  end
  return true
end

function EightQueen:compare(arg)
  return self.fitness > arg.fitness
end


function EightQueen:print()
  local line, spacer
  spacer = '–|–|–|–|–|–|–|–|–|'
  print(spacer)
  
  for y=8,1,-1 do
    line = y .. '|'
    for x=1,8 do
      if self.queens[x] == y then
        line = line .. 'X|'
      else
        line = line .. ' |'
      end
    end
    print(line)
    print(spacer)
  end
  line = ' |'
  for x=1,8 do
    line = line .. x .. '|'
  end
  print(line)
end

