-- KI P2 - 8-Queen-Problem

dofile("Class.lua")
dofile("Queue.lua")
dofile("EightQueens.lua")
dofile("GeneticEightQueenSolver.lua")
dofile("BacktrackingEightQueenSolver.lua")

-- Genetic Solver

--local populationStrings = {}

--for line in io.lines("population.txt") do 
--    table.insert(populationStrings, line)
--end

--for i=1, 25 do
--  local genetic = GeneticEightQueenSolver:new(populationStrings)
  
--  local t_start = os.time()
--  if genetic:run() then
--    local t_end = os.time()
--    file = io.open("result.txt", "a")
--    file:write(genetic.solution:toString() .. " in " .. t_end - t_start .. "s\n")
--    file:close()
--    genetic.solution:print()
--  end
--end

-- Backtracking Solver

local backtracking = BacktrackingEightQueenSolver:new()

if backtracking:run() then
  print("Success!")
  file = io.open("result_backtracking.txt", "w")
  for k,v in ipairs(backtracking.solutions) do
    file:write(k .. ":" .. v:toString() .. "\n")
    print("Solution " .. k .. " : " .. v:toString())
  end
  file:close()
else
  print("Failed")
end

