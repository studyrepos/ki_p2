-- Genetic Algorithm to solve EightQueenProblem

GeneticEightQueenSolver = {}
local GeneticEightQueenSolver_mt = Class(GeneticEightQueenSolver)

-- Constructor
function GeneticEightQueenSolver:new(initPopulation, iterations)
  local population = Queue:new(2)
  local iterations = iterations or 100
  local solution = {}
  
  if initPopulation then
    for i,v in ipairs(initPopulation) do
      local eq = EightQueen:new(v)
      eq:calcFitness()
      population:push(eq)
    end
    
  else
    for i=1, 100 do
      math.randomseed(os.time())
      local string = ''
      for j=1,8 do
        string = string .. math.random(1, 8)
      end
      local eq = EightQueen:new(string)
      eq:calcFitness()
      population:push(eq)
    end
  end

  return setmetatable( {population = population, iterations = iterations, solution = solution}, GeneticEightQueenSolver_mt)
end

-- Runner
-- needs better Selection...
function GeneticEightQueenSolver:run()  
  for g=1, self.iterations do
    local new_population = Queue:new(2)
    
    local pop = self.population:getData()
    for k=1,  table.maxn(pop) do
      math.randomseed(os.time() + k * g + 3)
      local a = math.random(1, table.maxn(pop) / 2)
      math.randomseed(os.time() + k * g + 5)
      local b = math.random(1, table.maxn(pop) / 2)
      while a == b do
        math.randomseed(os.time() + k * g + 11)
        b = math.random(1, table.maxn(pop) / 2)
      end
      -- calculate crossover
      local child = self:crossover(pop[a], pop[b])
      -- calculate random mutation
      self:mutation(child)
      -- calculate fitness
      child:calcFitness()
      
      if child.fitness == 28 then
        self.solution = child
        return true
      end
      new_population:push(child)
    end -- iterate population
    -- reset population to childs
    self.population = new_population
    -- sort population
    self.population:sort()
    print("Round " .. g .. " - Best Solution: " .. self.population.data[self.population.first]:toString())
  end -- iterations of genetic solver
  print("Failed")
  return false
end

function GeneticEightQueenSolver:crossover(parentA, parentB)
  math.randomseed(tonumber((parentA:queensToString()) * parentB.fitness) + (tonumber(parentB:queensToString()) * parentA.fitness))
  c = math.random(2,7)
  local child = EightQueen:new(
    string.sub(parentA:queensToString(), 1, c) .. string.sub(parentB:queensToString(), c+1, 8))
  return child
end

function GeneticEightQueenSolver:mutation(individual)
  math.randomseed(os.time() + tonumber(individual:queensToString()))
  if math.random(0,1) >= 0.5 then
    individual.queens[math.random(1,8)] = math.random(9,16) - 8
  end
end

function GeneticEightQueenSolver:print()
  for k,v in ipairs(self.population) do
    print('Population ' .. k, 'Fitness ' .. v.fitness)
    v:print()
  end
end

