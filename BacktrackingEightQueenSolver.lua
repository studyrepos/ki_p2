

BacktrackingEightQueenSolver = {}
local BacktrackingEightQueenSolver_mt = Class(BacktrackingEightQueenSolver)

function BacktrackingEightQueenSolver:new()
  local board = EightQueen:new('11111111')
  local solutions = {}
  return setmetatable ( {board = board, solutions = solutions}, BacktrackingEightQueenSolver_mt)
end


function BacktrackingEightQueenSolver:run(col)
  local col = col or 0
  
  -- capture first call and last backtrack
  if col == 0 then
    self:run(1)
    return true
  end
  
  if col > 8 then
    local solution = EightQueen:new(self.board:queensToString())
    solution:calcFitness()
    table.insert(self.solutions, solution)
    return false
  end
  
  for pos=1, 8 do
    if self.board:isSafe(col, pos) then
      self.board:moveQueen(col, pos)
      if self:run(col + 1) then
        return true
        end
    end
  end
  return false
end
